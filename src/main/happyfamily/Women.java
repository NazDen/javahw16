package happyfamily;


import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public final class Women extends Human implements HumanCreator {
    public Women(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
    }

    public Women(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Women(String name, String surname, long birthDate, int iq, HashMap<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Women() {
        
    }

    @Override
    void greetPet() {
        System.out.println("Привет, "+ getFamily().getPet().iterator().next().getNickname()+".");
    }

    public void makeUp(){
        System.out.println("Надо накраситса, а то хожу как чучело.");
    }

    @Override
    public Human bornChild(String boyName, String girlName) {
        Random random= new Random();
        int sex= random.nextInt(2);

        if (sex==0){
            Women girl = new Women();
            girl.setBirthDate(new Date().getTime());
            girl.setFamily(getFamily());
            girl.setName(girlName);
            girl.setSurname(getFamily().getFather().getSurname());
            girl.setIq((getFamily().getFather().getIq()+ getFamily().getMother().getIq())/2);
            girl.getFamily().addChild(girl);
            return girl;
        }
        else{
            Man boy = new Man();
            boy.setBirthDate(new Date().getTime());
            boy.setFamily(getFamily());
            boy.setName(boyName);
            boy.setSurname(getFamily().getFather().getSurname());
            boy.setIq((getFamily().getFather().getIq()+ getFamily().getMother().getIq())/2);
            boy.getFamily().addChild(boy);
            return boy;
        }
    }
}
