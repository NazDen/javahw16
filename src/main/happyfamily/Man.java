package happyfamily;

import java.text.ParseException;
import java.util.HashMap;

public final class Man extends Human {
    public Man(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, long birthDate, int iq, HashMap<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man() {
    }

    @Override
    void greetPet() {
        System.out.println("Хай, "+ getFamily().getPet().iterator().next().getNickname()+".");
    }

    public void repairCar(){
        System.out.println("Пойду ка я в гараж, посмотрю как моя ласточка пожываєт.");
    }
}
