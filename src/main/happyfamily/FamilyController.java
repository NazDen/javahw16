package happyfamily;

import java.util.List;
import java.util.Set;

public class FamilyController {

    private FamilyService familyService=new FamilyService();

    public FamilyController() {
    }

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers){
        return familyService.getFamiliesBiggerThan(numberOfMembers);
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers){
        return familyService.getFamiliesLessThan(numberOfMembers);
    }

    public  int countFamiliesWithMemberNumber(int numberOfMembers) {
        return familyService.countFamiliesWithMemberNumber(numberOfMembers);
    }

    public void createNewFamily(Women mother, Man father){
        familyService.createNewFamily(mother, father);
    }

    public void deleteFamilyByIndex(int index){
        familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family,String boyName,String girlName){
        return familyService.bornChild(family,boyName,girlName);
    }

    public Family adoptChild(Family family, Human human){
        return familyService.adoptChild(family,human);
    }

    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count(){
        return familyService.count();
    }

    public Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }

    public Set getPets(int index){
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet){
        familyService.addPet(index,pet);
    }

}
