package happyfamily;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Stream;

public class Console {

    private static final Scanner SCANNER = new Scanner(System.in);

    public void printCommand(){
        System.out.println("- 1. Заполнить тестовыми данными (автоматом создать несколько семей и сохранить их в базе)\n"+
                "- 2. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)\n"+
                "- 3. Отобразить список семей, где количество людей больше заданного\n"+
                "- 4. Отобразить список семей, где количество людей меньше заданного\n"+
                "- 5. Подсчитать количество семей, где количество членов равно\n"+
                "- 6. Создать новую семью\n"+
                "- 7. Удалить семью по индексу семьи в общем списке\n"+
                "- 8. Редактировать семью по индексу семьи в общем списке\n"+
                "- 9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)\n");
//    - 1. Заполнить тестовыми данными (автоматом создать несколько семей и сохранить их в базе)
//- 2. Отобразить весь список семей (отображает список всех семей с индексацией, начинающейся с 1)
//- 3. Отобразить список семей, где количество людей больше заданного
//  - запросить интересующее число
//- 4. Отобразить список семей, где количество людей меньше заданного
//  - запросить интересующее число
//- 5. Подсчитать количество семей, где количество членов равно
//  - запросить интересующее число
//- 6. Создать новую семью
//  - запросить имя матери
//  - запросить фамилию матери
//  - запросить год рождения матери
//  - запросить месяц рождения матери
//  - запросить день рождения матери
//  - запросить iq матери
//
//  - запросить имя отца
//  - запросить фамилию отца
//  - запросить год рождения отца
//  - запросить месяц рождения отца
//  - запросить день рождения отца
//  - запросить iq отца
//- 7. Удалить семью по индексу семьи в общем списке
//  - запросить порядковый номер семьи (ID)
//- 8. Редактировать семью по индексу семьи в общем списке
//  - 1. Родить ребенка
//    - запросить порядковый номер семьи (ID)
//    - запросить необходимые данные (какое имя дать мальчику, какое девочке)
//  - 2. Усыновить ребенка
//    - запросить порядковый номер семьи (ID)
//    - запросить необходимые данные (ФИО, год рождения, интеллект)
//  - 3. Вернуться в главное меню
//- 9. Удалить всех детей старше возраста (во всех семьях удаляются дети старше указанного возраста - будем считать, что они выросли)
//  - запросить интересующий возраст
    }

    public Women createWomen(){
        List<String>data=getData();
        Women women= null;
        try {
            women = new Women(data.get(0),data.get(1),data.get(4)+"/"+data.get(3)+"/"+data.get(2),Integer.parseInt(data.get(5)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return women;
    }

    private List<String> getData() {
        List<String> comands= Arrays.asList("Введите имя:","Введите фамилию:","Введите год рождения:","Введите месяц рождения цифрой:","Введите день рождения:","Введите iq:");
        List<String> data=new ArrayList<>();
        comands.forEach(s -> {System.out.println(s);
            data.add(SCANNER.next());});
        return data;
    }

    public Man createMan(){
        List<String>data=getData();
        Man man= null;
        try {
            man = new Man(data.get(0),data.get(1),data.get(4)+"/"+data.get(3)+"/"+data.get(2),Integer.parseInt(data.get(5)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return man;
    }


public void consoleRun(){
    FamilyController familyController = new FamilyController();
    String command;
do {
    printCommand();
    System.out.println("Введите номер команды: ");
    switch (command=SCANNER.next()){
        case "1":
            try {
                familyController.createNewFamily(new Women("Елена","Поп","14/02/1963", 68), new Man("Виктор","Поп","16/10/1955", 88));
                familyController.createNewFamily(new Women("Алла","Поп","11/05/1988",70),new Man("Николай","Бабочкин","01/02/1981",99));
                familyController.createNewFamily(new Women("Наташа","Дудкина","05/06/1968",80),new Man("Тарас", "Король", "11/02/1966",99));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            familyController.bornChild(familyController.getFamilyById(0), "Антон", "Вика");
            System.out.println("Семьи созданы.");
            break;
        case "2":
            familyController.displayAllFamilies();
            break;
        case "3":
            System.out.println("Введите количество людей:");
            familyController.getFamiliesBiggerThan(SCANNER.nextInt());
            break;
        case "4":
            System.out.println("Введите количество людей:");
            familyController.getFamiliesLessThan(SCANNER.nextInt());
            break;
        case "5":
            System.out.println("Введите количество людей:");
            System.out.println("Количество семей:\t"+familyController.countFamiliesWithMemberNumber(SCANNER.nextInt()));
            break;
        case "6":
            System.out.println("Введите данные матери");
            Women mother= createWomen();
            System.out.println("Введите данные отца");
            Man father= createMan();
            familyController.createNewFamily(mother,father);
            break;
        case "7":
            System.out.println("Введите порядковый номер семьи:");
            familyController.deleteFamilyByIndex(SCANNER.nextInt()-1);
            break;
        case "8":
            System.out.println("\t- 1. Родить ребенка\n"+
                               "\t- 2. Усыновить ребенка\n"+
                               "\t- 3. Вернуться в главное меню\n");
            System.out.println("Введите команду:");
            int commandCase8= SCANNER.nextInt();
            if (commandCase8== 1) {
                System.out.println("Введите порядковый номер семьи:");
                int index= SCANNER.nextInt()-1;
                System.out.println("Введите имя для девочки:");
                String girlName= SCANNER.next();
                System.out.println("Введите имя для мальчика:");
                String boyName= SCANNER.next();
                familyController.bornChild(familyController.getFamilyById(index),boyName,girlName);
                break;
            }else if(commandCase8== 2) {
                System.out.println("Введите порядковый номер семьи:");
                int index= SCANNER.nextInt()-1;

                System.out.println("Если хотите усиновит мальчика введите 1, если девочку введите 2:");
                int sex=SCANNER.nextInt();

                    if (sex==1) {
                        Man boy = createMan();
                        familyController.adoptChild(familyController.getFamilyById(index),boy);
                    }else if (sex==2) {
                        Women girl = createWomen();
                        familyController.adoptChild(familyController.getFamilyById(index),girl);
                    }
                    break;
            }else if (commandCase8== 3) {
                break;
            }
            break;
        case "9":
            System.out.println("Введите возраст:");
            int age= SCANNER.nextInt();
            familyController.deleteAllChildrenOlderThen(age);
            break;
        default:
            break;
    }
}while (!command.equals("exit"));

    }
}

