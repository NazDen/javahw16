package happyfamily;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

public class Human {

    static{
        System.out.println("Class "+ Human.class.getSimpleName()+" is loading.");
    }

    {
        System.out.println("New " + this.getClass().getSimpleName() +" class creating.");
    }

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String,String> schedule;
    private Family family;
    static private SimpleDateFormat birthFormat = new SimpleDateFormat("dd/MM/yyyy");

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    void greetPet(){
        System.out.println("Привет, "+ family.getPet().iterator().next().getNickname()+".");
    }

    void describePet(){
        String petTrickLevel;
        if (family.getPet().iterator().next().getTrickLevel()>50){
            petTrickLevel="очень хитрая";
        }
        else {
            petTrickLevel="почти не хитрая";
        }
        System.out.println("У меня есть "+family.getPet().iterator().next().getSpecies()+", ему "+family.getPet().iterator().next().getAge()+" лет, он "+petTrickLevel+".");
    }

    public String describeAge(){
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(new Date(birthDate));
        LocalDate birthDate= LocalDate.of(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1,calendar.get(Calendar.DAY_OF_MONTH));
        LocalDate now= LocalDate.now();
        Period period= Period.between(birthDate,now);
        String result= period.getYears()+" лет " + period.getMonths()+ " месяцов "+ period.getDays() + " дней";
        return result;
    }

    @Override
    protected void finalize(){
        System.out.println(this);
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName()+"{name= '"+name+"', surname= '"+surname+"', birthDate="+birthFormat.format(new Date(birthDate))+", iq="+iq+", schedule= "+ schedule+"}";
    }

    private String prettyFormat(){
        String result= "";
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate);
    }

    boolean feedPet(boolean timeToEat){
        Random rndm= new Random();
        int random=rndm.nextInt(101);
        boolean feeding;
        if (timeToEat){
            System.out.println("Хм... покормлю ка я "+family.getPet().iterator().next().getNickname()+".");
            feeding=true;
        }
        else {
            if (random<family.getPet().iterator().next().getTrickLevel()){
                System.out.println("Хм... покормлю ка я "+family.getPet().iterator().next().getNickname());
                feeding=true;

            }
            else{
                System.out.println("Думаю, "+family.getPet().iterator().next().getNickname()+" не голодна.");
                feeding=false;
            }
        }
        return feeding;
    }

    public Human(String name, String surname, String birthDate, int iq){
        this.name = name;
        this.surname = surname;
        try {
            this.birthDate = birthFormat.parse(birthDate).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.iq = iq;
    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, long birthDate, int iq, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {
    }


}
