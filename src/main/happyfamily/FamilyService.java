package happyfamily;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FamilyService {

  private FamilyDao familyDao= new CollectionFamilyDao();

    public FamilyService() {

    }
    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){return familyDao.getAllFamilies();}

  public void displayAllFamilies(){
      System.out.println();
      AtomicInteger atomicInteger = new AtomicInteger();
      familyDao.getAllFamilies().forEach(family -> System.out.println(atomicInteger.addAndGet(1)+"\t"+family.prettyFormat()));
      System.out.println();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfMembers){
        List<Family> result=familyDao.getAllFamilies().stream().filter(family -> family.countFamily()>numberOfMembers).collect(Collectors.toList());
        result.forEach(family -> System.out.println(family.prettyFormat()));
       return result;
    }

    public List<Family> getFamiliesLessThan(int numberOfMembers){
        List<Family> result = familyDao.getAllFamilies().stream().filter(family -> family.countFamily()<numberOfMembers).collect(Collectors.toList());
        result.forEach(family -> System.out.println(family.prettyFormat()));
        return result;
    }

    public  int countFamiliesWithMemberNumber(int numberOfMembers) {
        return (int) familyDao.getAllFamilies().stream().filter(family -> family.countFamily() == numberOfMembers).count();

    }

    public void createNewFamily(Women mother, Man father){
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
    }

    public void deleteFamilyByIndex(int index){
      familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family,String boyName,String girlName){
      familyDao.getFamilyByIndex(familyDao.getAllFamilies().indexOf(family)).getMother().bornChild(boyName, girlName);
      familyDao.saveFamily(family);
      return family;
  }

   public Family adoptChild(Family family,Human human){
       familyDao.getFamilyByIndex(familyDao.getAllFamilies().indexOf(family)).addChild(human);
       familyDao.saveFamily(family);
       return family;
   }

   public void deleteAllChildrenOlderThen(int age){
       familyDao.getAllFamilies().forEach(family -> family.getChildren().removeIf(human -> age< Integer.parseInt(human.describeAge().split(" ")[0])));
   }

   public int count(){
     return familyDao.getAllFamilies().size();
   }

   public Family getFamilyById(int index){
      return familyDao.getFamilyByIndex(index);
   }

   public Set getPets(int indexOfFamily){
    return familyDao.getFamilyByIndex(indexOfFamily
    ).getPet();
   }

   public void addPet(int indexOfFamily, Pet pet){
      familyDao.getFamilyByIndex(indexOfFamily).getPet().add(pet);
    }
}
