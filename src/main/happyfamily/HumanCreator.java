package happyfamily;

public interface HumanCreator {

     Human bornChild(String boyName, String girlName);

}
