package happyfamily;

import java.util.*;
import java.util.stream.Collectors;

public class Family  {

    static{
        System.out.println("Class "+ Family.class.getSimpleName()+" is loading.");
    }
    {
        System.out.println("New " + this.getClass().getSimpleName() +" class creating.");
    }

    private Women mother;
    private Man father;
    private List<Human> children;
    private Set<Pet> pet;


    Family(Women mother, Man father){
        this.mother= mother;
        this.father= father;
        mother.setFamily(this);
        father.setFamily(this);
        this.children= new ArrayList<>();
        this.pet= new HashSet<>();
    }

    public Women getMother() {
        return mother;
    }

    public void setMother(Women mother) {
        this.mother = mother;
    }

    public Man getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(this);
        super.finalize();
    }

    @Override
    public String toString(){

        return this.getClass().getSimpleName()+"{mother= "+mother+", father= "+father+", children= "+children+" pet= "+pet+"}";
    }


    public String prettyFormat(){
        String result= "family:\n" +
                "\t\tmother:\t"+mother+"\n"+
                "\t\tfather:\t"+father+"\n"+
                "\t\tchildren:\n"+"\t\t\t"+children.stream().map(Human::toString)
                .collect(Collectors.joining("\n\t\t\t"))+"\n"+
                "\t\tpets:\t"+pet;
        return result;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    public void addChild(Human child){
        children.add(child);
        child.setFamily(this);

//        Human[] temp= new Human[children.length+1];
//        temp[temp.length-1]=child;
//        for (int i = 0; i <children.length ; i++) {
//            temp[i]=children[i];
//        }
//        children=temp;
//        child.setFamily(this);
     }

     boolean deleteChild(Human smbdy){
      return   children.remove(smbdy);


//        boolean delete=false;
//        int indexChildren=0;
//        int indexTemp=0;
//        Human[] temp=new Human[children.length];
//        for (Human child:children) {
//            if (child.equals(smbdy)) {
//                indexChildren++;
//                child.setFamily(null);
//                delete = true;
//            } else {
//                temp[indexTemp] = children[indexChildren];
//                indexTemp++;
//                indexChildren++;
//            }
//        }
//         if (indexChildren!=indexTemp) {
//             children=Arrays.copyOfRange(temp,0,indexTemp);
//         }else {
//       children=temp;}
//        return delete;
     }

     int countFamily(){
         return 2 + children.size();
     }

}
