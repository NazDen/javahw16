package happyfamily;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Objects;

import static org.hamcrest.core.Is.is;

//@RunWith(Parameterized.class)
// public class ParameterizedTestFields{
//     @Parameterized.Parameter(0)
//    public String name;
//    @Parameterized.Parameter(1)
//    public String surname;
//    @Parameterized.Parameter(2)
//    public int year;
//    @Parameterized.Parameter(3)
//    public int iq;
//    @Parameterized.Parameter(4)
//    public String[][] schedule;
//}


public class HumanTest extends Human {

//    Human human;
//    String expectedResult;
//
//    public HumanTest(Human human, String expectedResult) {
//        this.human = human;
//        this.expectedResult = expectedResult;
//    }
//
//    @Parameterized.Parameters
//    public static Collections data(){
//        return Arrays.asList()
//    }

    @Test
    public void humanToStringWhenConstructorIsEmptyShouldReturnCorrectString() {
        //given
        Human human = new Human();
        System.out.println(human);
        //when
        String expectedResult="Human{name= 'null', surname= 'null', birthDate=01/01/1970, iq=0, schedule= null}";
        //then
        Assert.assertEquals(human.toString(),expectedResult);
    }

    @Test
    public void humanToStringWhenConstructorIsFullShouldReturnCorrectString() {
        //given
        Human human = new Human("Olia", "Zaiats",633042000000l,130,new HashMap<String, String>() {{put(DayOfWeek.MONDAY.name(),"Go to work, cook some dish");}});
        //when
        String expectedResult="Human{name= 'Olia', surname= 'Zaiats', birthDate=22/01/1990, iq=130, schedule= {MONDAY=Go to work, cook some dish}}";
        //then
        Assert.assertThat(human.toString(),is(expectedResult));
    }

    @Test
    public void humanHashCodeShouldReturnCorrectUniqueNumber() {
        //given
        Human human = new Human();
        int result= human.hashCode();
        //when
        int expectedResult= Objects.hash(human.getName(),human.getSurname(),human.getBirthDate());
        //then
        Assert.assertEquals(result,expectedResult);
    }

    @Test
    public void humanEqualsShouldReturnTrueIfReferencesAreOnTheSameObject() {
        //given
        Human human = new Human();
        //when
        Human human2= human;
        //then
        Assert.assertTrue(human.equals(human2));
        Assert.assertTrue(human2.equals(human));
    }

    @Test
    public void humanEqualsShouldReturnFalseIfObjectIsNull() {
        //given
        Human human = new Human();
        //when
        Human human2= null;
        //then
        Assert.assertFalse(human.equals(human2));
    }

    @Test
    public void humanEqualsShouldReturnFalseIfObjectClassIsDifferent() {
        //given
        Human human = new Human();
        //when
        Family family = new Family(new Women(), new Man());
        //then
        Assert.assertFalse(human.equals(family));
    }

    @Test
    public void humanEqualsShouldReturnTrueIfObjectPropertiesAreEqual() {
        //given
        Human human = new Human();
        //when
        Human human2= new Human();
        Human human3 = new Human();
        //then
        Assert.assertTrue(human.equals(human2));
        Assert.assertTrue(human2.equals(human3));
        Assert.assertTrue(human.equals(human3));
    }
}