package happyfamily;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpeciesTest {

    @Test
    public void specieToStringShouldReturnCorrectString() {
        //given
        Pet pet= new Pet() {
            @Override
            void respond() {
                System.out.println("fucking animals");
            }
        };
        //when
        String expectedResult= "UNKNOWN{canfly=false, numberOfLegs=0, hasFur=false}{nickname='null', age=0, trickLevel=0, habits=null}";
        //then
        assertEquals(expectedResult,pet.toString());

    }
}