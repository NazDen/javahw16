package happyfamily;


import org.junit.Test;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class FamilyTest {

    private Family family = new Family(new Women(), new Man());

    @Test
    public void familyToStringShouldReturnCorrectString() {
        //given
        //when
        System.out.println(family);
        String expectedResult="Family{mother= Women{name= 'null', surname= 'null', birthDate=01/01/1970, iq=0, schedule= null}, father= Man{name= 'null', surname= 'null', birthDate=01/01/1970, iq=0, schedule= null}, children= [] pet= []}";
        //then
        assertThat(expectedResult, is(family.toString()));
    }

    @Test
    public void familyAddChildShouldIncreaseChildrenByOne() {
        //given
        int countOfChildrenbeforeAdding= family.getChildren().size();
        //when
        family.addChild(new Human());
        //then
        assertThat(countOfChildrenbeforeAdding+1,is(family.getChildren().size()));
    }

    @Test
    public void familyAddChildShouldAddElementWithCorrectReferences() {
        //given
        //when
        Human human = new Human();
        family.addChild(human);
        //then
        assertTrue(human.getFamily()==family);
    }

    @Test
    public void familyAddChildShouldAddCorrectElement() {
        //given
        //when
        Human human = new Human();
        family.addChild(human);
        //then
//        assertEquals(human, family.getChildren().get(0));
        assertTrue(family.getChildren().contains(human));
    }

    @Test
    public void familyDeleteChildShouldDeleteCorrectChild() {
        //given
        Human human = new Human();
        List<Human> childrenBeforeAddingNewChild= family.getChildren();
        family.addChild(human);
        //when
        family.deleteChild(human);
        //then
        assertThat(childrenBeforeAddingNewChild,is(family.getChildren()));
    }

    @Test
    public void familyDeleteChildShouldNotDeleteChildIfChildNotExistInArray() {
        //given
        Human human = new Human("Oleg","Petrov",1995);
        family.addChild(human);
        List<Human> childrenAfterAddingElement= family.getChildren();
        //when
        Human human1 = new Human("Oleg","Petrov",1996);
        family.deleteChild(human1);
        //then
        assertEquals(childrenAfterAddingElement,family.getChildren());
    }


    @Test
    public void familyCountFamilyReturnCorrectFamilyMembersCount() {
        //given
        //when
        //then
        assertEquals(family.countFamily(),2);
    }

    @Test
    public void familyHashCodeShouldReturnCorrectUniqueNumber() {
        //given
        int result= family.hashCode();
        //when
        int expectedResult= Objects.hash(family.getFather(),family.getMother());
        //then
        assertEquals(result,expectedResult);
    }

    @Test
    public void familyEqualsShouldReturnTrueIfReferencesAreOnTheSameObject() {
        //given
        //when
        Family family2= family;
        //then
        assertTrue(family2.equals(family));
        assertTrue(family.equals(family2));
    }

    @Test
    public void familyEqualsShouldReturnFalseIfObjectIsNull() {
        //given
        //when
        Family family2= null;
        //then
        assertFalse(family.equals(family2));
    }

    @Test
    public void familyEqualsShouldReturnFalseIfObjectClassIsDifferent() {
        //given
        //when
        Human human = new Human();
        //then
        assertFalse(family.equals(human));
    }

    @Test
    public void familyEqualsShouldReturnTrueIfObjectPropertiesAreEqual() {
        //given
        //when
        Family family2 = new Family(new Women(), new Man());
        Family family3 = new Family(new Women(), new Man());
        //then
        assertTrue(family.equals(family2));
        assertTrue(family2.equals(family3));
        assertTrue(family.equals(family3));
    }

}
