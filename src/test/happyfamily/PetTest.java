package happyfamily;

import org.junit.Test;

import java.util.HashSet;
import java.util.Objects;

import static org.junit.Assert.*;

public class PetTest {

private Pet pet= new Fish("Olia",2,100, new HashSet<String>() {{add("кушать"); add("спать");}});

    @Test
    public void petToStringShouldReturnCorrectString() {
        //given
        //when
        String expectedResult= "FISH{canfly=false, numberOfLegs=0, hasFur=false}{nickname='Olia', age=2, trickLevel=100, habits=[кушать, спать]}";
        //then
        assertEquals(expectedResult,pet.toString());
    }

    @Test
    public void petHashCodeShouldReturnCorrectUniqueNumber() {
        //given
        int result= pet.hashCode();
        //when
        int expectedResult= Objects.hash(pet.getNickname(),pet.getAge());
        //then
        assertEquals(result,expectedResult);
    }

    @Test
    public void petEqualsShouldReturnTrueIfReferencesAreOnTheSameObject() {
        //given
        //when
        Pet pet2= pet;
        //then
        assertTrue(pet.equals(pet2));
        assertTrue(pet2.equals(pet));
    }

    @Test
    public void petEqualsShouldReturnFalseIfObjectIsNull() {
        //given
        //when
        Pet pet2= null;
        //then
        assertFalse(pet.equals(pet2));
    }

    @Test
    public void petEqualsShouldReturnFalseIfObjectClassIsDifferent() {
        //given
        //when
        Human human = new Human();
        //then
        assertFalse(pet.equals(human));
    }

    @Test
    public void petEqualsShouldReturnTrueIfObjectPropertiesAreEqual() {
        //given
        //when
        Pet pet2 = new Fish("Olia",2,99, new HashSet<String>(){{add("не кушать");add("не спать");}});
        Pet pet3 = new Fish("Olia",2,1, new HashSet<String>(){{add("много кушать");add("много спать");}});

        //then
        assertTrue(pet.equals(pet2));
        assertTrue(pet2.equals(pet3));
        assertTrue(pet.equals(pet3));
    }
}